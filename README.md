# [Marketing] User persona

:::info
:bulb: Persona is a method of profiling target users that marketers can use to analyze and develop strategies.
[https://marcelomogrovejo.gitlab.io/my-ios-portfolio/](https://marcelomogrovejo.gitlab.io/my-ios-portfolio/)
:::

## :eyes:Who?

:::success
What are the characteristics and basic information of your target customers?
:::

:small_blue_diamond:**Name:**  
:small_blue_diamond:**Age:**  
:small_blue_diamond:**Gender:**  
:small_blue_diamond:**Highest Level of Education:**  
:small_blue_diamond:**Location:**  
:small_blue_diamond:**Job title:**  
:small_blue_diamond:**Job description:**  

## :thought_balloon:What?

:::success
You can imagine the situation in which this customer will encounter problems. Your value is in being ready to help them when the situation arises.
:::

### :small_blue_diamond:Goals:

- 
-

### :small_blue_diamond:Challenges:

-
-

### :small_blue_diamond:Actions:

-
-